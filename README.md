# Parsing Exploration
Just a place to play a bit around with parsing. For that i will construct a small regex engine that can be used for the parser, most likely be an early parser.

## Regex engine
First we construct an NFA based on thompson's construction out of an s-expression based regex description like the following:
* empty -> `()` or `nil`
* char -> `#\c`
* string -> `"s"`
* alternation -> `(or item ... items)`
* concatenate -> `(item ... items)`
* kleene star -> `(* item ... items)`
  * the kleene star have an build in implicit concatenate on its items

We also support the one or more (`+`) and none or one (`?`) by just translating them to the above expressions:
* `(+ items)` -> `(items (* items))`
* `(? items)` -> `(or (items) nil)`


