(ql:quickload :trivia)

(defpackage :regex
  (:use :cl :trivia))

(in-package :regex)
(use-package :trivia)

; get useable debug info
(declaim (optimize (debug 3)))

(defstruct fsa start end trans)

(defun counter-generator (counter)
  (lambda () (incf counter)))

(defvar next-state (counter-generator 0))

(defmethod re-2-fsa (re)
  (match re
    ((list* '* items)
     (star (re-2-fsa items)))
    ((list* 'or items)
     (reduce #'alt (mapcar #'re-2-fsa items)))
    ((list* '+ items)
     (re-2-fsa `(,items (* ,items))))
    ((list* '? items)
     (re-2-fsa `(or ,items nil)))
    ((list* items)
     (reduce #'cat (mapcar #'re-2-fsa items)))))

(defmethod re-2-fsa ((null null))
  (let ((start (funcall next-state))
        (end   (funcall next-state)))
    (make-fsa :start start
              :end   end
              :trans (list (list start '() end)))))

(defmethod re-2-fsa ((char character))
  (let ((start (funcall next-state))
        (end   (funcall next-state)))
    (make-fsa :start start
              :end   end
              :trans (list (list start char end)))))

(defmethod re-2-fsa ((string string))
  (reduce #'cat (map 'list #'re-2-fsa string)))

(defun cat (fsa1 fsa2)
  (make-fsa :start (fsa-start fsa1)
            :end   (fsa-end   fsa2)
            :trans (concatenate 'list
                    (fsa-trans fsa1)
                    (fsa-trans fsa2)
                    (list (list (fsa-end fsa1) nil (fsa-start fsa2))))))

(defun alt (fsa1 fsa2)
  (let ((start (funcall next-state))
        (end   (funcall next-state)))
    (make-fsa :start start
              :end   end
              :trans (concatenate 'list
                      (fsa-trans fsa1)
                      (fsa-trans fsa2)
                      (list
                       (list start nil (fsa-start fsa1))
                       (list start nil (fsa-start fsa2))
                       (list (fsa-end fsa1) nil end)
                       (list (fsa-end fsa2) nil end))))))

(defun star (fsa)
  (let ((start (funcall next-state))
        (end   (funcall next-state)))
    (make-fsa :start start
              :end   end
              :trans (concatenate 'list
                      (fsa-trans fsa)
                      (list
                       (list start nil (fsa-start fsa))
                       (list (fsa-end fsa) nil start)
                       (list start nil end))))))
